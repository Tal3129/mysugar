package com.ohadkatri.kcalculator;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CardAdapter extends ArrayAdapter<String> {
    List<String> items;
    int resource;

    public CardAdapter(@NonNull Context context, int resource, @NonNull List<String> objects) {
        super(context, resource, objects);
        this.items = objects;
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        String current = items.get(position);
        View view = inflater.inflate(resource, null);
        TextView label = view.findViewById(R.id.cardItemLabel);
        label.setText(current);
        return view;
    }
}
