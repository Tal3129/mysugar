package com.ohadkatri.kcalculator;

import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.concurrent.locks.Lock;

public class DatabaseManager {
    private static DatabaseManager instance = null;

    public DatabaseReference mealsRef;
    public ArrayList<Meal> meals = new ArrayList<>();
    
    public DatabaseReference generalFoodRef;
    public ArrayList<Food> generalFood = new ArrayList<>();

    public DatabaseReference customFoodRef;
    public ArrayList<Food> customFood = new ArrayList<>();
    
    public DatabaseReference sugarChecksRef;
    public ArrayList<SugarCheck> sugarChecks = new ArrayList<>();
    
    public DatabaseReference recipesRef;
    public ArrayList<Recipe> recipes = new ArrayList<>();

    private int initializedCount = 0;
    private static final int LIST_COUNT = 5;
    public final Object initializedNotifier = new Object();

    public static DatabaseManager getInstance() {
        if (instance == null) {
            instance = new DatabaseManager();
        }
        return instance;
    }

    private DatabaseManager() {
        initializeMeals();
        initializeSugarChecks();
        initializeGeneralFood();
        initializeCustomFood();
        initializeRecipes();
    }



    public boolean isInitialized() {
        return initializedCount >= LIST_COUNT;
    }

    private void addInitialized() {
        initializedCount++;
        synchronized (initializedNotifier) {
            if (isInitialized()) {
                initializedNotifier.notifyAll();
            }
        }
    }

    public void waitForInitialization() {
        try {
            synchronized (initializedNotifier) {
                if (!isInitialized()) {
                    initializedNotifier.wait();
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void initializeMeals() {
        mealsRef = getUserReference().child("meals");
        mealsRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                DatabaseManager.this.meals.clear();
                for (DataSnapshot businessSnapshot : dataSnapshot.getChildren()) {
                    Meal meal = businessSnapshot.getValue(Meal.class);
                    DatabaseManager.this.meals.add(meal);
                }
                addInitialized();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }
    private void initializeSugarChecks() {
        sugarChecksRef = getUserReference().child("sugarChecks");
        sugarChecksRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                DatabaseManager.this.sugarChecks.clear();
                for (DataSnapshot businessSnapshot : dataSnapshot.getChildren()) {
                    SugarCheck sugarCheck = businessSnapshot.getValue(SugarCheck.class);
                    DatabaseManager.this.sugarChecks.add(sugarCheck);
                }
                addInitialized();
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    private void initializeGeneralFood() {
        generalFoodRef = FirebaseDatabase.getInstance().getReference("generalFood");
        generalFoodRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                DatabaseManager.this.generalFood.clear();
                for (DataSnapshot businessSnapshot : dataSnapshot.getChildren()) {
                    Food food = businessSnapshot.getValue(Food.class);
                    DatabaseManager.this.generalFood.add(food);
                }
                addInitialized();
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    private void initializeCustomFood() {
        customFoodRef= getUserReference().child("customFood");
        customFoodRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                DatabaseManager.this.generalFood.clear();
                for (DataSnapshot businessSnapshot : dataSnapshot.getChildren()) {
                    Food food = businessSnapshot.getValue(Food.class);
                    DatabaseManager.this.customFood.add(food);
                }
                addInitialized();
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    private void initializeRecipes() {
        recipesRef = FirebaseDatabase.getInstance().getReference("recipes");
        recipesRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                DatabaseManager.this.recipes.clear();
                for (DataSnapshot businessSnapshot : dataSnapshot.getChildren()) {
                    Recipe recipe = businessSnapshot.getValue(Recipe.class);
                    DatabaseManager.this.recipes.add(recipe);
                }
                addInitialized();
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    public DatabaseReference getUserReference() {
        return FirebaseDatabase.getInstance().getReference("users").child(FirebaseAuth.getInstance().getCurrentUser().getUid());
    }
}
