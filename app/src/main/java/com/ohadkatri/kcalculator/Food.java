package com.ohadkatri.kcalculator;

public class Food {
    private String name;
    private int carbsPerHundredGrams;

    public Food(){}

    public Food(String name, int carbsPerHundredGrams) {
        this.name = name;
        this.carbsPerHundredGrams = carbsPerHundredGrams;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCarbsPerHundredGrams() {
        return carbsPerHundredGrams;
    }

    public void setCarbsPerHundredGrams(int carbsPerHundredGrams) {
        this.carbsPerHundredGrams = carbsPerHundredGrams;
    }

    @Override
    public String toString() {
        return "Food{" +
                "name='" + name + '\'' +
                ", CarbsPerHundredGrams='" + carbsPerHundredGrams + '\'' +
                '}';
    }
}
