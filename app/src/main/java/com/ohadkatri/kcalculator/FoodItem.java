package com.ohadkatri.kcalculator;

public class FoodItem {
    private String foodName;
    private int grams;
    private int carbs;

    public FoodItem() { }

    public FoodItem(String foodName, int grams, int carbs) {
        this.foodName = foodName;
        this.grams = grams;
        this.carbs = carbs;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public int getGrams() {
        return grams;
    }

    public void setGrams(int grams) {
        this.grams = grams;
    }

    public int getCarbs() {
        return carbs;
    }

    public void setCarbs(int carbs) {
        this.carbs = carbs;
    }

    @Override
    public String toString() {
        return "FoodItem{" +
                "foodName='" + foodName + '\'' +
                ", grams=" + grams +
                ", carbs=" + carbs +
                '}';
    }
}
