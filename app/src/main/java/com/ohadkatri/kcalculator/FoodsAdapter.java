package com.ohadkatri.kcalculator;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ohadkatri.kcalculator.activities.AddMealActivity;

import java.util.List;

public class FoodsAdapter extends ArrayAdapter<Food> {
    private Context context;
    private int resources;
    private List<Food> foodList;

    public FoodsAdapter(Context context, int resources, List<Food> foodList) {
        super(context, resources, foodList);
        this.context = context;
        this.resources = resources;
        this.foodList = foodList;
    }

    public List<Food> getFoodList() {
        return foodList;
    }

    public void setFoodList(List<Food> foodList) {
        this.foodList = foodList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(resources, null);

        TextView textViewFoodName = view.findViewById(R.id.textViewFoodName);
        TextView textViewFoodCarbs = view.findViewById(R.id.textViewFoodCarbs);
        final ImageView toggleFood = view.findViewById(R.id.imageViewToggleFood);

        final Food currentFood = foodList.get(position);

        textViewFoodName.setText(currentFood.getName());
        textViewFoodCarbs.setText(currentFood.getCarbsPerHundredGrams() + " פחמימות למאה גרם");
        toggleFood.setOnClickListener(v -> {
            // Remove food item if in list
            for (FoodItem foodItem : AddMealActivity.currentMeal.getFoodItems()) {
                if (foodItem.getFoodName().equals(currentFood.getName())) {
                    AddMealActivity.currentMeal.getFoodItems().remove(foodItem);
                    notifyDataSetChanged();
                    return;
                }
            }
            // Add food item if not
            startAddFoodItemDialog(currentFood);
        });

        toggleFood.setImageResource(AddMealActivity.currentMeal.hasFood(currentFood) ? android.R.drawable.ic_delete : android.R.drawable.ic_input_add);
        return view;
    }

    private void startAddFoodItemDialog(Food currentFood) {
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.edit_food_dialog);
        TextView textViewName = dialog.findViewById(R.id.textViewDialogFoodName);
        final EditText editTextGrams = dialog.findViewById(R.id.editTextDialogGrams);
        Button buttonAdd = dialog.findViewById(R.id.buttonDialogAddFood);
        textViewName.setText(currentFood.getName());
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.show();

        buttonAdd.setOnClickListener(v1 -> {
            int grams = Integer.valueOf(editTextGrams.getText().toString());
            int carbs = (int) (currentFood.getCarbsPerHundredGrams() * grams / 100.0);
            AddMealActivity.currentMeal.getFoodItems().add(new FoodItem(currentFood.getName(), grams, carbs));
            notifyDataSetChanged();
            dialog.cancel();
        });
    }
}
