package com.ohadkatri.kcalculator;

import java.util.ArrayList;

public class Meal {
    private String id;
    private long mealTime;
    private ArrayList<FoodItem> foodItems;

    public Meal() {
        foodItems = new ArrayList<>();
    }

    public Meal(String id, long mealTime, ArrayList<FoodItem> foodItems) {
        this.id = id;
        this.mealTime = mealTime;
        this.foodItems = foodItems;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getMealTime() {
        return mealTime;
    }

    public void setMealTime(long mealTime) {
        this.mealTime = mealTime;
    }

    public ArrayList<FoodItem> getFoodItems() {
        return foodItems;
    }

    public void setFoodItems(ArrayList<FoodItem> foodItems) {
        this.foodItems = foodItems;
    }

    public int getCarbsSum() {
        int sum = 0;
        for (FoodItem item : foodItems) {
            sum += item.getCarbs();
        }
        return sum;
    }

    public boolean hasFood(Food food) {
        return this.foodItems.stream().anyMatch(foodItem -> foodItem.getFoodName().equals(food.getName()));
    }

    @Override
    public String toString() {
        return "Meal{" +
                "id='" + id + '\'' +
                ", mealTime=" + mealTime +
                ", foodItems=" + foodItems +
                '}';
    }
}
