package com.ohadkatri.kcalculator;

import java.io.Serializable;

public class Recipe implements Serializable {
    private String category;
    private String title;
    private String body;

    public Recipe(String title, String content, String category) {
        this.category = category;
        this.title = title;
        this.body = content;
    }

    public Recipe() {
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body.replace("\\n", "\n");
    }
}
