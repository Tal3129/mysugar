package com.ohadkatri.kcalculator;


import java.io.Serializable;
import java.util.ArrayList;

public class RecipeCategory implements Serializable {
    private String displayName;
    private ArrayList<Recipe> recipes;

    public RecipeCategory() {
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public ArrayList<Recipe> getRecipes() {
        return recipes;
    }

    public void setRecipes(ArrayList<Recipe> recipes) {
        this.recipes = recipes;
    }
}
