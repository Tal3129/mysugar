package com.ohadkatri.kcalculator;

public class SugarCheck {
    private String id;
    private long checkTime;
    private int sugarLevel;

    public SugarCheck() {}

    public SugarCheck(String id, long checkTime, int sugarLevel) {
        this.id = id;
        this.checkTime = checkTime;
        this.sugarLevel = sugarLevel;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public long getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(long checkTime) {
        this.checkTime = checkTime;
    }

    public int getSugarLevel() {
        return sugarLevel;
    }

    public void setSugarLevel(int sugarLevel) {
        this.sugarLevel = sugarLevel;
    }

    @Override
    public String toString() {
        return "SugarCheck{" +
                "id='" + id + '\'' +
                ", checkTime=" + checkTime +
                ", sugarLevel=" + sugarLevel +
                '}';
    }
}
