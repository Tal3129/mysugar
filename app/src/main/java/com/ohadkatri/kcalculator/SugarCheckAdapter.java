package com.ohadkatri.kcalculator;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class SugarCheckAdapter extends ArrayAdapter<SugarCheck>  {
    List<SugarCheck> checks = new ArrayList<>();
    int resources;
    Context context;
    final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
    final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM");

    public SugarCheckAdapter(@NonNull Context context, int resource, @NonNull ArrayList<SugarCheck> checks) {
        super(context, resource, checks);
        this.checks = checks;
        this.checks.sort(Comparator.comparing(SugarCheck::getCheckTime).reversed());
        this.resources = resource;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        SugarCheck current = checks.get(position);
        View view = inflater.inflate(resources, null);
        TextView sugarLevel = view.findViewById(R.id.sugarLevelTV);
        TextView checkDate = view.findViewById(R.id.checkDate);
        TextView checkTime = view.findViewById(R.id.checkTime);

        sugarLevel.setText(String.valueOf(current.getSugarLevel()));
        checkDate.setText(dateFormat.format(new Date(current.getCheckTime())));
        checkTime.setText(timeFormat.format(new Date(current.getCheckTime())));
        return view;
    }
}
