package com.ohadkatri.kcalculator;

import android.content.Context;
import android.content.SharedPreferences;

import com.ohadkatri.kcalculator.activities.LoginScreen;
import com.oryanhosh.andronotifier.AndroNotification;
import com.oryanhosh.andronotifier.AndroNotifier;

import java.util.Calendar;

import static android.content.Context.MODE_PRIVATE;

public class SugarNotifications {
    public static final String KEY_SUGAR_CHECK = "SUGAR_CHECK_REMINDER";
    public static final String KEY_DAILY_GRAPH = "DAILY_GRAPH_REMINDER";
    public static final String KEY_WEEKLY_GRAPH = "WEEKLY_GRAPH_REMINDER";

    public static void scheduleSugarCheckNotification(Context context, long notificationTime) {
        AndroNotification notification = new AndroNotification(
                KEY_SUGAR_CHECK,
                notificationTime,
                "תזכורת לבדיקת סוכר",
                "תזכורת לבדיקת סוכר",
                "תזכורת",
                R.drawable.icon,
                LoginScreen.class
        );
        AndroNotifier.addNotification(context, notification);
    }

    public static void scheduleDailyGraphNotification(Context context, long notificationTime) {
        AndroNotification notification = new AndroNotification(
                KEY_DAILY_GRAPH,
                notificationTime,
                "תזכורת לדוח יומי",
                "תזכורת לדוח יומי",
                "תזכורת",
                R.drawable.icon,
                LoginScreen.class
        );
        AndroNotifier.addNotification(context, notification);
    }

    public static void scheduleWeeklyGraphNotification(Context context, long notificationTime) {
        AndroNotification notification = new AndroNotification(
                KEY_WEEKLY_GRAPH,
                notificationTime,
                "תזכורת לדוח שבועי",
                "תזכורת  לדוח שבועי",
                "תזכורת",
                R.drawable.icon,
                LoginScreen.class
        );
        AndroNotifier.addNotification(context, notification);
    }

    public static void putReminderTime(Context context, String prefsKey, long time, boolean enabled) {
        SharedPreferences.Editor editor = context.getSharedPreferences(prefsKey, MODE_PRIVATE).edit();
        editor.putLong("time", time);
        editor.putBoolean("enabled", enabled);
        editor.apply();
    }

    // Returns null if disabled
    public static Calendar getRemainderTime(Context context, String prefsKey) {
        SharedPreferences prefs = context.getSharedPreferences(prefsKey, MODE_PRIVATE);
        long time = prefs.getLong("time", 0);
        boolean enabled = prefs.getBoolean("enabled",  false);
        if (!enabled) {
            return null;
        }

        Calendar notificationTime = Calendar.getInstance();
        notificationTime.setTimeInMillis(time);
        return notificationTime;
    }

    // Reschedule all notifications whose time has passed
    public static void rescheduleNotifications(Context context) {
        Calendar sugarTime = SugarNotifications.getRemainderTime(context, SugarNotifications.KEY_SUGAR_CHECK);
        if (sugarTime != null && sugarTime.getTimeInMillis() < Calendar.getInstance().getTimeInMillis()) {
            sugarTime.add(Calendar.DAY_OF_YEAR, 1);
            SugarNotifications.scheduleSugarCheckNotification(context, sugarTime.getTimeInMillis());
        }

        Calendar dailyGraphTime = SugarNotifications.getRemainderTime(context, SugarNotifications.KEY_DAILY_GRAPH);
        if (dailyGraphTime != null && dailyGraphTime.getTimeInMillis() < Calendar.getInstance().getTimeInMillis()) {
            dailyGraphTime.add(Calendar.DAY_OF_YEAR, 1);
            SugarNotifications.scheduleDailyGraphNotification(context, dailyGraphTime.getTimeInMillis());
        }

        Calendar weeklyGraphTime = SugarNotifications.getRemainderTime(context, SugarNotifications.KEY_WEEKLY_GRAPH);
        if (weeklyGraphTime != null && weeklyGraphTime.getTimeInMillis() < Calendar.getInstance().getTimeInMillis()) {
            weeklyGraphTime.add(Calendar.DAY_OF_YEAR, 7);
            SugarNotifications.scheduleWeeklyGraphNotification(context, weeklyGraphTime.getTimeInMillis());
        }
    }
}
