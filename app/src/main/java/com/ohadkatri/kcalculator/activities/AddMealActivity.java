package com.ohadkatri.kcalculator.activities;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DatabaseReference;
import com.ohadkatri.kcalculator.DatabaseManager;
import com.ohadkatri.kcalculator.Food;
import com.ohadkatri.kcalculator.FoodItem;
import com.ohadkatri.kcalculator.FoodsAdapter;
import com.ohadkatri.kcalculator.Meal;
import com.ohadkatri.kcalculator.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;

public class AddMealActivity extends AppCompatActivity {
    public static Meal currentMeal = new Meal();
    private Button buttonAddMeal, buttonAddCustom;
    private EditText editTextCustomName, editTextCustomCals;
    private ListView listViewFoods;
    private ImageView imageViewMeal;
    private FoodsAdapter adapter;
    private ArrayList<Food> allFoods;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_meal);

        // Set action bar title
        getSupportActionBar().setTitle("ארוחה");
        getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

        // Get all foods from database
        allFoods = new ArrayList<>();
        allFoods.addAll(DatabaseManager.getInstance().generalFood);
        allFoods.addAll(DatabaseManager.getInstance().customFood);

        // Initialize views
        buttonAddMeal = findViewById(R.id.buttonAddMeal);
        listViewFoods = findViewById(R.id.listviewFoods);
        imageViewMeal = findViewById(R.id.imageViewTotalMeal);
        buttonAddCustom = findViewById(R.id.buttonAddCustom);
        editTextCustomName = findViewById(R.id.editTextAddCustomName);
        editTextCustomCals = findViewById(R.id.editTextAddCustomCals);

        // Set the adapter of the foods listview
        adapter = new FoodsAdapter(AddMealActivity.this, R.layout.food_item, allFoods);
        listViewFoods.setAdapter(adapter);

        // Add custom food button - add the food to list and DB.
        buttonAddCustom.setOnClickListener(view -> {
            if (editTextCustomCals.getText().toString().isEmpty() || editTextCustomName.getText().toString().isEmpty()) {
                Toast.makeText(this, "הכנס ערכים לפני הוספת מאכל", Toast.LENGTH_SHORT).show();
                return;
            }
            int cals = Integer.parseInt(editTextCustomCals.getText().toString());
            String addedFoodName = editTextCustomName.getText().toString();
            editTextCustomCals.setText("");
            editTextCustomName.setText("");

            // If food already exists, don't add it.
            if (allFoods.stream().anyMatch(food -> food.getName().equals(addedFoodName))) {
                Toast.makeText(this, addedFoodName + " כבר ברשימה", Toast.LENGTH_SHORT).show();
                return;
            }

            // Add food to DB
            Food newFood = new Food(addedFoodName, cals);
            DatabaseReference customFoodRef = DatabaseManager.getInstance().customFoodRef;
            customFoodRef.child(Objects.requireNonNull(customFoodRef.push().getKey())).setValue(newFood);

            // Wait for DB to update, then update adapter.
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            adapter.notifyDataSetChanged();
        });

        // Show meal
        imageViewMeal.setOnClickListener(view -> {
            showMeal(AddMealActivity.this, currentMeal.getFoodItems());
        });

        // Add meal
        buttonAddMeal.setOnClickListener(view -> {
            if (!currentMeal.getFoodItems().isEmpty()) {
                String id = DatabaseManager.getInstance().mealsRef.push().getKey();
                currentMeal.setId(id);
                currentMeal.setMealTime(Calendar.getInstance().getTimeInMillis());
                DatabaseManager.getInstance().mealsRef.child(id).setValue(currentMeal);
                Toast.makeText(AddMealActivity.this, "הארוחה נוספה!", Toast.LENGTH_SHORT).show();
                currentMeal = new Meal();
                finish();
            } else {
                Toast.makeText(this, "הארוחה שלך ריקה...", Toast.LENGTH_SHORT).show();
            }
        });
    }

    // Show current meal details
    private void showMeal(Context context, ArrayList<FoodItem> mealFoodItems) {
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.meal_dialog);
        TextView textViewName = dialog.findViewById(R.id.textViewDialogFoodList);
        String details = "הארוחה שלך ריקה...";
        if (!mealFoodItems.isEmpty()) {
            int totalCals = 0;
            details = "";
            for (FoodItem foodItem : mealFoodItems) {
                totalCals += foodItem.getCarbs();
                details += foodItem.getGrams() + " גרם " + foodItem.getFoodName() + ": " + foodItem.getCarbs() + " פחמימות\n";
            }
            details += "\nסך הכל פחמימות: " + totalCals + "\n\n";
        }
        textViewName.setText(details);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.show();
    }
}
