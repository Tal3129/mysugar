package com.ohadkatri.kcalculator.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.ohadkatri.kcalculator.DatabaseManager;
import com.ohadkatri.kcalculator.R;
import com.ohadkatri.kcalculator.SugarCheck;
import com.ohadkatri.kcalculator.SugarCheckAdapter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class AddSugarCheckActivity extends AppCompatActivity {
    EditText sugarLevelView;
    ImageView addCheck;
    final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
    final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.mm.yyyy");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_sugar_check);

        // Set action bar title
        getSupportActionBar().setTitle("בדיקת סוכר");
        getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

        // Initialize views
        sugarLevelView = findViewById(R.id.editTextSugarLevel);
        addCheck = findViewById(R.id.buttonAddSugarLevel);
        ListView checksList = findViewById(R.id.sugarCheckListView);

        checksList.setAdapter(new SugarCheckAdapter(this, R.layout.sugarcheck_row, DatabaseManager.getInstance().sugarChecks));
        setCurrentTime();

        // Add sugar check to database
        addCheck.setOnClickListener(view -> {
            String id = DatabaseManager.getInstance().sugarChecksRef.push().getKey();
            int sugarLevel = Integer.parseInt(sugarLevelView.getText().toString());
            SugarCheck sugarCheck = new SugarCheck(
                    id,
                    Calendar.getInstance().getTimeInMillis(),
                    sugarLevel
            );
            DatabaseManager.getInstance().sugarChecksRef.child(id).setValue(sugarCheck);
            Toast.makeText(this, "בדיקת סוכר נוספה!", Toast.LENGTH_SHORT).show();
            finish();
        });
    }

    private void setCurrentTime() {
        TextView currentTime = findViewById(R.id.textViewTime);
        TextView currentDate = findViewById(R.id.textViewDate);
        currentTime.setText(timeFormat.format(new Date()));
        currentDate.setText(dateFormat.format(new Date()));
    }
}
