package com.ohadkatri.kcalculator.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.LabelFormatter;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.ohadkatri.kcalculator.DatabaseManager;
import com.ohadkatri.kcalculator.Meal;
import com.ohadkatri.kcalculator.R;
import com.ohadkatri.kcalculator.SugarCheck;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class GraphActivity extends AppCompatActivity {
    static boolean weekly = true;
    GraphView graph;
    TextView textViewTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);

        graph = findViewById(R.id.graph);
        textViewTitle = findViewById(R.id.textViewGraphTitle);

        if (weekly) {
            textViewTitle.setText("גרף סוכר פחמימות - שבועי");
        } else {
            textViewTitle.setText("גרף סוכר פחמימות - יומי");
        }

        BarGraphSeries<DataPoint> carbsSeries = createCarbsGraph();
        LineGraphSeries<DataPoint> sugarSeries = createSugarGraph();

        applyGraphs(carbsSeries, sugarSeries);
    }

    private void applyGraphs(BarGraphSeries<DataPoint> carbsSeries, LineGraphSeries<DataPoint> sugarSeries) {
        if (weekly) {
            graph.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(GraphActivity.this));
       }
        else {
            graph.getGridLabelRenderer().setLabelFormatter(new LabelFormatter() {
                @Override
                public String formatLabel(double value, boolean isValueX) {
                    if (isValueX) {
                        value *= 30;
                        return String.format("%02d", (int)(value / 60)) + ":" + String.format("%02d", (int)(value % 60));
                    }
                     else {
                        return Double.toString(value);
                    }
                }

                @Override
                public void setViewport(Viewport viewport) {

                }
            });
        }
        graph.getGridLabelRenderer().setNumHorizontalLabels(3); // only 4 because of the space

        double minX = Math.min(carbsSeries.getLowestValueX(), sugarSeries.getLowestValueX());
        double manX = Math.max(carbsSeries.getHighestValueX(), sugarSeries.getHighestValueX());
        double maxY = Math.max(carbsSeries.getHighestValueY(), sugarSeries.getHighestValueY());
        graph.getViewport().setMinX(minX);
        graph.getViewport().setMaxX(manX);

        graph.getViewport().setMaxY(maxY * 1.1);
        graph.animate();
        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setYAxisBoundsManual(true);
        graph.getGridLabelRenderer().setHumanRounding(false);
    }

    private BarGraphSeries<DataPoint> createCarbsGraph() {
        ArrayList<DataPoint> dataPointsCarb = new ArrayList<>();
        Calendar currentDay = Calendar.getInstance();


        if (weekly) {
            // If this is a weekly graph - for each day over last week, sum the carbs of meals in that day. Add as a datapoint.
            currentDay.add(Calendar.DATE, -6);
            IntStream.range(0, 7).forEach(day -> {
                int totalCarbsThisDay = DatabaseManager.getInstance().meals.stream().filter(meal -> isAtDay(meal, currentDay)).mapToInt(Meal::getCarbsSum).sum();
                dataPointsCarb.add(new DataPoint(currentDay.getTime(), totalCarbsThisDay));
                currentDay.add(Calendar.DATE, 1);
            });
        } else {
            // If this is a daily graph - go over each meal, and as a datapoint. Normalize the meal time for normal-looking graph.
            for (Meal m : DatabaseManager.getInstance().meals) {
                if (isAtDay(m, currentDay)) {
                    Calendar mealCal = Calendar.getInstance();
                    mealCal.setTimeInMillis(m.getMealTime());
                    dataPointsCarb.add(new DataPoint((mealCal.get(Calendar.HOUR_OF_DAY) * 60 + mealCal.get(Calendar.MINUTE)) / 30, m.getCarbsSum()));
                }
            }
        }
        Object[] objNamesCarbs = dataPointsCarb.toArray();
        DataPoint[] datapintsarrayCabs = Arrays.copyOf(objNamesCarbs, objNamesCarbs.length, DataPoint[].class);
        BarGraphSeries<DataPoint> seriesCarbs = new BarGraphSeries<>(datapintsarrayCabs);
        seriesCarbs.setValueDependentColor(data -> Color.rgb((int) data.getX() * 255 / 4, (int) Math.abs(data.getY() * 255 / 6), 100));
        seriesCarbs.setDrawValuesOnTop(true);
        seriesCarbs.setSpacing(20);
        seriesCarbs.setValuesOnTopColor(Color.BLACK);
        graph.addSeries(seriesCarbs);
        return seriesCarbs;
    }

    private LineGraphSeries<DataPoint> createSugarGraph() {
        ArrayList<DataPoint> dataPointsSugar = new ArrayList<>();
        long minTimeSugar = DatabaseManager.getInstance().sugarChecks.get(0).getCheckTime();
        for (SugarCheck sugarCheck : DatabaseManager.getInstance().sugarChecks.stream().sorted(Comparator.comparing(SugarCheck::getCheckTime)).collect(Collectors.toList())) {

            Calendar sugarCheckCal = Calendar.getInstance();
            sugarCheckCal.setTimeInMillis(sugarCheck.getCheckTime());
            Calendar graphStartTime = Calendar.getInstance();
            if (weekly) {
                graphStartTime.add(Calendar.DATE, -6);
            } else {
                graphStartTime.add(Calendar.DATE, -1);
            }

            // Add a datapoint for each sugar check after the start time. Normalize daily check times.
            if (sugarCheckCal.getTimeInMillis() < graphStartTime.getTimeInMillis()) {
                continue;
            }
            if (weekly) {
                dataPointsSugar.add(new DataPoint(sugarCheckCal.getTime(), sugarCheck.getSugarLevel()));
            }
            else {
                Calendar checkCal = Calendar.getInstance();
                checkCal.setTimeInMillis(sugarCheck.getCheckTime());
                dataPointsSugar.add(new DataPoint((checkCal.get(Calendar.HOUR_OF_DAY) * 60 + checkCal.get(Calendar.MINUTE)) / 30, sugarCheck.getSugarLevel()));
            }
            if (minTimeSugar > sugarCheck.getCheckTime()) {
                minTimeSugar = sugarCheck.getCheckTime();
            }
        }
        Object[] objNamesSugar = dataPointsSugar.toArray();
        DataPoint[] datapointsarraySugar = Arrays.copyOf(objNamesSugar, objNamesSugar.length, DataPoint[].class);
        LineGraphSeries<DataPoint> seriesSugar = new LineGraphSeries<>(datapointsarraySugar);
        graph.addSeries(seriesSugar);
        return seriesSugar;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.graph_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.graphDaily) {
            weekly = false;
        } else if (item.getItemId() == R.id.graphWeekly) {
            weekly = true;
        } else {
            return super.onOptionsItemSelected(item);
        }

        Intent intent = new Intent(getApplicationContext(), GraphActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        recreate();
        return true;
    }

    private boolean isAtDay(Meal meal, Calendar day) {
        Calendar mealCal = Calendar.getInstance();
        mealCal.setTimeInMillis(meal.getMealTime());
        return day.get(Calendar.DAY_OF_YEAR) == mealCal.get(Calendar.DAY_OF_YEAR) && day.get(Calendar.YEAR) == mealCal.get(Calendar.YEAR);
    }

}
