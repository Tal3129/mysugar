package com.ohadkatri.kcalculator.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.ohadkatri.kcalculator.R;
import com.ohadkatri.kcalculator.SugarNotifications;

import java.util.Calendar;

public class LoginScreen extends AppCompatActivity implements View.OnClickListener {
    private Button buttonLogin;
    private EditText editTextEmail, editTextPassword;
    private TextView textViewCreateAccount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);

        buttonLogin = findViewById(R.id.buttonLogin);
        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPassword = findViewById(R.id.editTextPassword);
        textViewCreateAccount = findViewById(R.id.textViewCreateAccount);
        buttonLogin.setOnClickListener(this);
        textViewCreateAccount.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == buttonLogin) {
            String email = editTextEmail.getText().toString();
            String password = editTextPassword.getText().toString();
            loginToAccount(email, password);

        } else if (v == textViewCreateAccount) {
            startActivity(new Intent(this, RegisterActivity.class));
        }
    }

    // Use firebase authentication on given email and password. Start MainActivity if successful.
    private void loginToAccount(String email, String password) {
        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                startActivity(new Intent(LoginScreen.this, MainActivity.class));
                finish();
            } else {
                Toast.makeText(LoginScreen.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        SugarNotifications.rescheduleNotifications(this);

        //if user is already in, start main.
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
    }




}
