package com.ohadkatri.kcalculator.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.ohadkatri.kcalculator.DatabaseManager;
import com.ohadkatri.kcalculator.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Button buttonAddMeal, buttonViewCarbs, buttonAddSugarCheck, buttonRecipes;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonAddMeal = findViewById(R.id.buttonAddMeal);
        buttonViewCarbs = findViewById(R.id.buttonReports);
        buttonAddSugarCheck = findViewById(R.id.buttonAddSugarCheck);
        buttonRecipes = findViewById(R.id.buttonRecipes);

        buttonViewCarbs.setOnClickListener(this);
        buttonAddSugarCheck.setOnClickListener(this);
        buttonAddMeal.setOnClickListener(this);
        buttonRecipes.setOnClickListener(this);

        // Wait for database initialization
        dialog = ProgressDialog.show(this, "",
                "טוען נתונים...", true);
        Thread thread = new Thread(() -> {
            DatabaseManager.getInstance().waitForInitialization();
            runOnUiThread(() -> {
                dialog.dismiss();
            });
        });
        thread.start();
    }


    @Override
    public void onClick(View v) {
        if (v == buttonAddMeal) {
            startActivity(new Intent(this, AddMealActivity.class));
        } else if (v == buttonViewCarbs) {
            startActivity(new Intent(this, GraphActivity.class));
        } else if (v == buttonAddSugarCheck) {
            startActivity(new Intent(this, AddSugarCheckActivity.class));
        } else if (v == buttonRecipes) {
            startActivity(new Intent(this, RecipeCategoriesActivity.class));
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.settings_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.itemSettings) {
            startActivity(new Intent(MainActivity.this, SettingsActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
