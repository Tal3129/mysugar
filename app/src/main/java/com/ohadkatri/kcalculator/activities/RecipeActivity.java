package com.ohadkatri.kcalculator.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.ohadkatri.kcalculator.R;
import com.ohadkatri.kcalculator.Recipe;


public class RecipeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe);
        Bundle extras = getIntent().getExtras();
        Recipe recipe = (Recipe) extras.getSerializable("recipe");

        getSupportActionBar().setTitle("מתכונים - " + recipe.getTitle());
        getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

        TextView recipeTitle = findViewById(R.id.recipeTitle);
        TextView recipeBody = findViewById(R.id.recipeBody);

        recipeTitle.setText(recipe.getTitle());
        recipeBody.setText(Html.fromHtml(recipe.getBody(), Html.FROM_HTML_MODE_LEGACY));
    }
}