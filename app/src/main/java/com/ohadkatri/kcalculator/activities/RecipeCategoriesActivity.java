package com.ohadkatri.kcalculator.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.ohadkatri.kcalculator.CardAdapter;
import com.ohadkatri.kcalculator.DatabaseManager;
import com.ohadkatri.kcalculator.R;
import com.ohadkatri.kcalculator.Recipe;
import com.ohadkatri.kcalculator.RecipeCategory;

import java.util.List;
import java.util.stream.Collectors;

public class RecipeCategoriesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview);
        getSupportActionBar().setTitle("מתכונים");
        getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);


        ListView categoryList = findViewById(R.id.categoryListview);

        // Show a list of one of each recipe category
        List<Recipe> recipes = DatabaseManager.getInstance().recipes;
        List<String> categories = recipes.stream().map(Recipe::getCategory).distinct().collect(Collectors.toList());
        categoryList.setAdapter(new CardAdapter(this, R.layout.my_card_item, categories));

        categoryList.setOnItemClickListener((parent, view, position, id) -> {
            Intent recipesIntent = new Intent(this, RecipeCategoryItemsActivity.class);
            recipesIntent.putExtra("category", categories.get(position));
            startActivity(recipesIntent);
        });
    }
}