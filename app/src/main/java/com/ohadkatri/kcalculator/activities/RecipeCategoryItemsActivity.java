package com.ohadkatri.kcalculator.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.ohadkatri.kcalculator.CardAdapter;
import com.ohadkatri.kcalculator.DatabaseManager;
import com.ohadkatri.kcalculator.R;
import com.ohadkatri.kcalculator.Recipe;

import java.util.List;
import java.util.stream.Collectors;

public class RecipeCategoryItemsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview);
        ListView categoryList = findViewById(R.id.categoryListview);

        String category = getIntent().getStringExtra("category");

        getSupportActionBar().setTitle("מתכונים - " + category);
        getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

        // Show a list of all titles of recipes with given category
        List<Recipe> recipes = DatabaseManager.getInstance().recipes.stream().filter(recipe -> recipe.getCategory().equals(category)).collect(Collectors.toList());
        List<String> recipeTitles = recipes.stream().map(Recipe::getTitle).collect(Collectors.toList());
        categoryList.setAdapter(new CardAdapter(this, R.layout.my_card_item, recipeTitles));

        categoryList.setOnItemClickListener((parent, view, position, id) -> {
            Intent recipesIntent = new Intent(this, RecipeActivity.class);
            recipesIntent.putExtra("recipe", recipes.get(position));
            startActivity(recipesIntent);
        });
    }
}