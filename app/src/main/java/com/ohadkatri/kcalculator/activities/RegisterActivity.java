package com.ohadkatri.kcalculator.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.ohadkatri.kcalculator.R;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {
    private Button buttonCreateAccount;
    private EditText editTextName, editTextEmail, editTextPass1, editTextPass2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        buttonCreateAccount = findViewById(R.id.buttonCreateAccount);
        editTextEmail = findViewById(R.id.editTextRegisterEmail);
        editTextName = findViewById(R.id.editTextRegisterName);
        editTextPass1 = findViewById(R.id.editTextRegisterPasswor1);
        editTextPass2 = findViewById(R.id.editTextRegisterPassword2);
        buttonCreateAccount.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == buttonCreateAccount) {
            String pass1 = editTextPass1.getText().toString();
            String pass2 = editTextPass2.getText().toString();
            String name = editTextName.getText().toString();
            String email = editTextEmail.getText().toString();

            if (pass1.equals(pass2)) {
                tryToCreateUser(name, email, pass1);
            } else {
                Toast.makeText(this, "סיסמה ואימות לא תואמות", Toast.LENGTH_SHORT).show();

            }
        }
    }

    // Create a user with given details with firebase authentication
    private void tryToCreateUser(final String name, final String email, final String password) {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("יוצר חשבון...");
        dialog.show();
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password).addOnCompleteListener(task -> {
                    dialog.dismiss();
                    if (task.isSuccessful()) {
                        UserProfileChangeRequest changeRequest = new UserProfileChangeRequest.Builder()
                                .setDisplayName(name).build();
                        FirebaseAuth.getInstance().getCurrentUser().updateProfile(changeRequest).addOnCompleteListener(task1 -> {
                            Toast.makeText(RegisterActivity.this, "החשבון נוצר", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(RegisterActivity.this, MainActivity.class));
                            finish();
                        });
                    } else if (task.getException() instanceof FirebaseAuthUserCollisionException) {
                        Toast.makeText(RegisterActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(RegisterActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }
}
