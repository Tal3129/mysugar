package com.ohadkatri.kcalculator.activities;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import com.ohadkatri.kcalculator.R;
import com.ohadkatri.kcalculator.SugarNotifications;
import com.oryanhosh.andronotifier.AndroNotifier;

import java.util.Calendar;

public class SettingsActivity extends AppCompatActivity {
    Button saveBtn;
    Calendar sugarCheckTime = Calendar.getInstance();
    Calendar dailyGraphTime = Calendar.getInstance();
    Calendar weeklyGraphTime = Calendar.getInstance();

    SwitchCompat sugarCheckNotificationSwitch;
    SwitchCompat dailyGraphNotificationSwitch;
    SwitchCompat weeklyGraphNotificationSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        sugarCheckNotificationSwitch = findViewById(R.id.sugarCheckNotificationSwitch);
        dailyGraphNotificationSwitch = findViewById(R.id.dailyGraphNotificationSwitch);
        weeklyGraphNotificationSwitch = findViewById(R.id.weeklyGraphNotificationSwitch);
        saveBtn = findViewById(R.id.buttonSaveMinutes);

        loadInitialConfiguration();
        setCheckListeners();
        setSaveListener();
    }


    // Set checked and current reminder-time if there is saved time in prefs.
    private void loadInitialConfiguration() {
        Calendar sugarCheckSavedTime = SugarNotifications.getRemainderTime(this, SugarNotifications.KEY_SUGAR_CHECK);
        if (sugarCheckSavedTime != null) {
            sugarCheckNotificationSwitch.setChecked(true);
            sugarCheckTime = sugarCheckSavedTime;
        }

        Calendar dailyGraphSavedTime = SugarNotifications.getRemainderTime(this, SugarNotifications.KEY_DAILY_GRAPH);
        if (dailyGraphSavedTime != null) {
            dailyGraphNotificationSwitch.setChecked(true);
            dailyGraphTime = dailyGraphSavedTime;
        }

        Calendar weeklyGraphSavedTime = SugarNotifications.getRemainderTime(this, SugarNotifications.KEY_WEEKLY_GRAPH);
        if (weeklyGraphSavedTime != null) {
            weeklyGraphNotificationSwitch.setChecked(true);
            weeklyGraphTime = weeklyGraphSavedTime;
        }
    }

    // On check of each switch, start a time picker dialog. On time picked, set the matching calender.
    private void setCheckListeners() {
        sugarCheckNotificationSwitch.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (isChecked) {
                TimePickerDialog dialog = new TimePickerDialog(this, (timePicker, pickedHour, pickedMinute) -> {
                    sugarCheckTime.set(Calendar.HOUR_OF_DAY, pickedHour);
                    sugarCheckTime.set(Calendar.MINUTE, pickedMinute);
                }, sugarCheckTime.get(Calendar.HOUR_OF_DAY), sugarCheckTime.get(Calendar.MINUTE), true);
                dialog.show();
            }
        });

        dailyGraphNotificationSwitch.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (isChecked) {
                TimePickerDialog dialog = new TimePickerDialog(this, (timePicker, pickedHour, pickedMinute) -> {
                    dailyGraphTime.set(Calendar.HOUR_OF_DAY, pickedHour);
                    dailyGraphTime.set(Calendar.MINUTE, pickedMinute);
                }, dailyGraphTime.get(Calendar.HOUR_OF_DAY), dailyGraphTime.get(Calendar.MINUTE), true);
                dialog.show();
            }
        });

        weeklyGraphNotificationSwitch.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (isChecked) {
                TimePickerDialog dialog = new TimePickerDialog(this, (timePicker, pickedHour, pickedMinute) -> {
                    weeklyGraphTime.set(Calendar.HOUR_OF_DAY, pickedHour);
                    weeklyGraphTime.set(Calendar.MINUTE, pickedMinute);
                }, weeklyGraphTime.get(Calendar.HOUR_OF_DAY), weeklyGraphTime.get(Calendar.MINUTE), true);
                dialog.show();
            }
        });
    }

    // On save click, save current calenders to prefs and reschedule the notifications.
    private void setSaveListener() {
        saveBtn.setOnClickListener(view -> {
            if (sugarCheckNotificationSwitch.isChecked()) {
                SugarNotifications.putReminderTime(this, SugarNotifications.KEY_SUGAR_CHECK, sugarCheckTime.getTimeInMillis(), true);
                SugarNotifications.scheduleSugarCheckNotification(this, sugarCheckTime.getTimeInMillis());
            } else {
                SugarNotifications.putReminderTime(this, SugarNotifications.KEY_SUGAR_CHECK, 0, false);
                AndroNotifier.removeNotification(this, SugarNotifications.KEY_SUGAR_CHECK);
            }

            if (dailyGraphNotificationSwitch.isChecked()) {
                SugarNotifications.putReminderTime(this, SugarNotifications.KEY_DAILY_GRAPH, dailyGraphTime.getTimeInMillis(), true);
                SugarNotifications.scheduleDailyGraphNotification(this, dailyGraphTime.getTimeInMillis());
            } else {
                SugarNotifications.putReminderTime(this, SugarNotifications.KEY_DAILY_GRAPH, 0, false);
                AndroNotifier.removeNotification(this, SugarNotifications.KEY_DAILY_GRAPH);
            }

            if (weeklyGraphNotificationSwitch.isChecked()) {
                SugarNotifications.putReminderTime(this, SugarNotifications.KEY_WEEKLY_GRAPH, weeklyGraphTime.getTimeInMillis(), true);
                SugarNotifications.scheduleWeeklyGraphNotification(this, weeklyGraphTime.getTimeInMillis());
            } else {
                SugarNotifications.putReminderTime(this, SugarNotifications.KEY_WEEKLY_GRAPH, 0, false);
                AndroNotifier.removeNotification(this, SugarNotifications.KEY_WEEKLY_GRAPH);
            }

            // If client has chosen times in the past, reschedule them to next iteration (day or week).
            SugarNotifications.rescheduleNotifications(this);

            Toast.makeText(this, "ההגדרות נשמרו", Toast.LENGTH_SHORT).show();
        });
    }

}
